# Full Stack Web Development Bootcamp ( React JS + Django) 2021

Contents: 

<br><br>
### 01. Introduction/:

- 01.01. Introduction.mp4
- 01.02. Let's get connected! Join the Learning Community.html
- 01.03.1 HOW-TO-GUIDE.html
- 01.03.1 HOW-TO-GUIDE.pdf
- 01.04. Course Project Files Overview.mp4
- 01.05. Course project GitHub Repository.html
- 01.06. COURSE STRUCTURE.html
- 01.07. ALL MY TOP COURSES.html

<br><br>
### 02. Introduction to the Web Development/:

- 02.01. Frontend vs Backend.mp4

<br><br>
### 03. SHELL and TERMINAL/:

- 03.01. Shell vs Terminal.mp4
- 03.02. Command Line Interface.mp4
- 03.03. User and root directories.mp4
- 03.04. Current and parent directories.mp4
- 03.05. Navigation between directories.mp4
- 03.06. Listing files.mp4
- 03.07. Creating and removing files and directories.mp4
- 03.08. Executable files.mp4
- 03.09. Multiple tabs.mp4

<br><br>
### 04. VISUAL STUDIO CODE/:

- 04.01. Installing VS Code.mp4
- 04.02. Getting familiar with VS Code User Interface.mp4
- 04.03. Opening existing project in the VS Code.mp4
- 04.04. Editing files and navigating between files.mp4
- 04.05. Creating and deleting new files and folders.mp4
- 04.06. Quick Setup and Themes.mp4
- 04.07. Changing Icon Theme.mp4
- 04.08. Programming Languages Support.mp4
- 04.09. Executing commands in VS Code.mp4
- 04.10. Integrated Terminal.mp4
- 04.11. Extensions.mp4
- 04.12. Modifying User Settings.mp4
- 04.13. Modifying Workspace Settings.mp4
- 04.14. Interactive Playground.mp4
- 04.15. Code Snippets.mp4
- 04.16. Emmet.mp4
- 04.17. Shortcuts in the VS Code.mp4
- 04.18. VS Code Summary.mp4

<br><br>
### 05. GIT and GITHUB/:

- 05.01. Git and GitHub Overview.mp4
- 05.02. Installing Git.mp4
- 05.03. Initializing new Git Repository.mp4
- 05.04. Git Object Types.mp4
- 05.05. Hashes of the Git Objects.mp4
- 05.06. Adding changes to the Staging Area.mp4
- 05.07. Configuring Git author name and email.mp4
- 05.08. Committing changes.mp4
- 05.09. Exploring commits history.mp4
- 05.10. Basic Git Commands.mp4
- 05.11. Checking out commits and branches.mp4
- 05.12. File tracking statuses in Git.mp4
- 05.13. Staging and Committing changes using one command.mp4
- 05.14. Branches and merging of the branches.mp4
- 05.15. Branches merging in action.mp4
- 05.16. Exploring commits tree using graph option.mp4
- 05.17. GitHub and repository hosting services Overview.mp4
- 05.18. Creating remote repository at GitHub.mp4
- 05.19. Pushing from the local repository to remote.mp4
- 05.20. Committing at GitHub and pulling to the local repository.mp4
- 05.21. Exploring and reading Git Objects.mp4
- 05.22. Reading contents of the HEAD file.mp4
- 05.23. Git Crash Course Summary.mp4

<br><br>
### 06. JAVASCRIPT/:

- 06.01. JavaScript Crash Course Overview.mp4
- 06.02. Creating course project and starting Live Server.mp4
- 06.03. Variables declaration using let and const.mp4
- 06.04. Naming conventions in JavaScript.mp4
- 06.05. Adding Comments.mp4
- 06.06. Primitive Types and typeof operator.mp4
- 06.07. Plus operator and types coersion.mp4
- 06.08. What are Operators in JavaScript.mp4
- 06.09. JavaScript Expressions.mp4
- 06.10. Comparison Operators.mp4
- 06.11. Logical Operators.mp4
- 06.12. Falsy values.mp4
- 06.13. Continue with logical operators.mp4
- 06.14. Short-circuit evaluation in AND and OR operators.mp4
- 06.15. Ternary Operator.mp4
- 06.16. Objects creation and modification.mp4
- 06.17. Object is Reference type in JavaScript.mp4
- 06.18. Square brackets notation, keys and values of the object.mp4
- 06.19. Arrays and proving that arrays are objects.mp4
- 06.20. Adding elements to the arrays.mp4
- 06.21. Prototype methods and removing elements from the array.mp4
- 06.22. Functions.mp4
- 06.23. Function scope and variables.mp4
- 06.24. Function parameters.mp4
- 06.25. Default values for the parameters and operations inside of the function.mp4
- 06.26. Arrow function expressions.mp4
- 06.27. Syntax options in the arrow functions.mp4
- 06.28. Callback functions.mp4
- 06.29. Order of operations in JavaScript.mp4
- 06.30. Methods in the objects.mp4
- 06.31. Methods syntax variations.mp4
- 06.32. Properties vs Methods in the objects.mp4
- 06.33. Arrays Destructuring.mp4
- 06.34. Objects Destructuring.mp4
- 06.35. Object destructuring of the function parameters.mp4
- 06.36. Function parameters destructuring and default values.mp4
- 06.37. Template String Literals.mp4
- 06.38. If and if else statements.mp4

<br><br>
### 07. NPM/:

- 07.01. NPM Crash Course Overview and NPM Installation.mp4
- 07.02. Initializing NPM in the new project.mp4
- 07.03. Updating NPM.mp4
- 07.04. Overview of the package file.mp4
- 07.05. Running NPM Scripts.mp4
- 07.06. Installing first NPM dependency.mp4
- 07.07. Overview of the node_modules folder.mp4
- 07.08. NPM installs also other dependencies.mp4
- 07.09. NPM packages versioning.mp4
- 07.10. Semver - Major, Minor and Patch versions.mp4
- 07.11. Creating and running basic Node.js application.mp4
- 07.12. Creating custom NPM scripts.mp4
- 07.13. Node.js project Summary.mp4
- 07.14. Limit updates of the packages by using tilda and caret signs.mp4
- 07.15. How semver impacts NPM packages installation process.mp4
- 07.16. Bootstraping React application using create-react-app.mp4
- 07.17. React app Overview.mp4
- 07.18. Running different NPM scripts in the React application.mp4
- 07.19. Creating VUE.js project at the codesandbox.mp4
- 07.20. Development Dependencies.mp4
- 07.21. NPM key features Summary.mp4
- 07.22. NPM Crash Course Summary.mp4

<br><br>
### 08. REACT/:

- 08.01. React Crash Course Overview.mp4
- 08.02. Getting familiar with React application and JSX.mp4
- 08.03. Inserting HTML elements into the JSX.mp4
- 08.04. Styling JSX elements.mp4
- 08.05. Functional Components in React.mp4
- 08.06. Inserting JavaScript code into the JSX.mp4
- 08.07. Creating new React Component.mp4
- 08.08. Using React Components in the other Components.mp4
- 08.09. Component props in the React.mp4
- 08.10. Destructuring Component props.mp4
- 08.11. Using Component props in the JSX.mp4
- 08.12. Reusing same React Component.mp4
- 08.13. Using Template String Literal as value of the Component prop.mp4
- 08.14. Trying to use regular variable for the state changes.mp4
- 08.15. React state must be used instead of the regular variables.mp4
- 08.16. Introducing useState React hook for the state management.mp4
- 08.17. Changing Component State via hook.mp4
- 08.18. Parent and child components are re-rendered upon each state change.mp4
- 08.19. React state and props Summary.mp4
- 08.20. React Crash Course Summary and Challenge.mp4

<br><br>
### 09. PYTHON/:

- 09.01. Python Crash Course Overview.mp4
- 09.02. Hello from Python.mp4
- 09.03. Base Types.mp4
- 09.04. Conversion between types.mp4
- 09.05. Falsy values.mp4
- 09.06. Naming conventions for variables and constants.mp4
- 09.07. Variables and Dynamic Typing.mp4
- 09.08. Base types are copied by value.mp4
- 09.09. Incrementing and Decrementing variables.mp4
- 09.10. Lists.mp4
- 09.11. Tuples.mp4
- 09.12. Dictionaries.mp4
- 09.13. Copy by Reference.mp4
- 09.14. Sets.mp4
- 09.15. Container Types Summary.mp4
- 09.16. Installing and configuring Code Runner in the VS Code.mp4
- 09.17. Functions.mp4
- 09.18. Positional arguments.mp4
- 09.19. Keyword (Named) arguments.mp4
- 09.20. Gathering all arguments to the tuple.mp4
- 09.21. Iteration over container type values using for in loop.mp4
- 09.22. Function kwargs.mp4
- 09.23. Function Description.mp4
- 09.24. Comparison Operators.mp4
- 09.25. Logical operator AND.mp4
- 09.26. Short-circuit evaluation.mp4
- 09.27. Logical operator OR.mp4
- 09.28. Logical operator NOT.mp4
- 09.29. Chaining operators and precedence.mp4
- 09.30. If conditional statements.mp4
- 09.31. Strings formatting.mp4
- 09.32. Exceptions handling using try except.mp4
- 09.33. Python Modules.mp4

<br><br>
### 10. DOCKER/:

- 10.01. Docker Overview.mp4
- 10.02. Installing Docker Desktop or Docker Engine.mp4
- 10.03. Virtual Machines vs Docker Containers.mp4
- 10.04. Virtual Machines Architecture.mp4
- 10.05. How I use Virtual Machines.mp4
- 10.06. Docker Containers Architecture.mp4
- 10.07. How Docker is running on different Operating Systems.mp4
- 10.08. Container processes and resources.mp4
- 10.09. Docker Components Overview.mp4
- 10.10. Docker Client.mp4
- 10.11. Docker Server.mp4
- 10.12. Docker Host.mp4
- 10.13. Docker Image.mp4
- 10.14. Docker Container.mp4
- 10.15. Docker Repository.mp4
- 10.16. Docker Registry.mp4
- 10.17. Docker Components Summary.mp4
- 10.18. Docker Commands vs Management Commands.mp4
- 10.19. Alternative Commands.mp4
- 10.20. Basic Container and Images commands.mp4
- 10.21. Cleaning up my Docker setup.mp4
- 10.22. Pulling images from the Docker Hub.mp4
- 10.23. What is Docker Image.mp4
- 10.24. Creating new Container from the Image.mp4
- 10.25. What is CMD in the Docker Image.mp4
- 10.26. What is Docker Container.mp4
- 10.27. What is ports mapping.mp4
- 10.28. Enabling ports mapping for the NGINX container.mp4
- 10.29. Environment variables for containers.mp4
- 10.30. Volumes and volumes mapping.mp4
- 10.31. Enabling volumes mapping for the NGINX container.mp4
- 10.32. Running applications inside of the containers.mp4
- 10.33. What is Dockerfile.mp4
- 10.34. Creating Dockerfile.mp4
- 10.35. Launching container based on the custom image.mp4
- 10.36. Trying to connect Python and Mongo containers.mp4
- 10.37. Attaching containers to the custom bridge network.mp4
- 10.38. Docker Compose and YAML.mp4
- 10.39. Launching services using Docker Compose.mp4
- 10.40. Writing documents to the database.mp4
- 10.41. Enabling Ports Mapping in the Docker Compose.mp4
- 10.42. Using Volumes in the Docker Compose.mp4
- 10.43. Docker Crash Course Summary.mp4
- 10.44. Publishing course project files to the GitHub.mp4

<br><br>
### 11. PROJECT START - Initial frontend app Overview/:

- 11.01. Installing Visual Studio Code.mp4
- 11.02. Visual Studio Code Setup.mp4
- 11.03. Overview of the initial Application version.mp4
- 11.04. How to create new React Application.mp4
- 11.05. Installing Node along with NPM and NPX.mp4

<br><br>
### 12. Creating React application and how React works/:

- 12.01. BEGIN - Creating React Application using create-react-app.mp4
- 12.02. Starting React Application.mp4
- 12.03. Structure of the Frontend app.mp4
- 12.04. Reinstalling npm dependencies.mp4
- 12.05. Creating and serving optimized build of the Frontend app.mp4
- 12.06. Basic Frontend Application Implementation Steps.mp4
- 12.07. Cleaning up default React application.mp4
- 12.08. How React works and what is JSX.mp4
- 12.09. React Functional Components.mp4

<br><br>
### 13. Initializing Git and creating remote GitHub repository/:

- 13.01. Installing and Configuring Git.mp4
- 13.02.1 01. END - initial frontend app - 8afcfc4.zip
- 13.02. END - Creating first commit.mp4
- 13.03.1 02. BEGIN - remove .eslintcache from index - 8afcfc4.zip
- 13.03.2 02. END - remove .eslintcache from index - 5e60337.zip
- 13.03.3 03. BEGIN - git ignore .eslintcache - 5e60337.zip
- 13.03.4 03. END - git ignore .eslintcache - 2dfcb86.zip
- 13.03. BEGIN and END - Adding eslintcache file to the gitignore.mp4
- 13.04. Publishing repository to the GitHub.mp4

<br><br>
### 14. React props and creation of the Header and Search components/:

- 14.01.1 04. BEGIN - changed favicon - 2dfcb86.zip
- 14.01.2 04. END - changed favicon - a51e5dd.zip
- 14.01. BEGIN and END - Changing Favicon.mp4
- 14.02.1 05. BEGIN - initial header component - a51e5dd.zip
- 14.02. BEGIN - Creating Header component.mp4
- 14.03.1 05. END - initial header component - ab7357d.zip
- 14.03. END - What are React props.mp4
- 14.04.1 06. BEGIN - controlled search component - ab7357d.zip
- 14.04. BEGIN - Plan for creation of the Search component.mp4
- 14.05. Creating Search component with input form.mp4
- 14.06. Styling Search component.mp4
- 14.07. Submission of the search form.mp4
- 14.08. Controlled Search component.mp4
- 14.09.1 06. END - controlled search component - 4dcf62d.zip
- 14.09. END - Search Component Summary.mp4
- 14.10. Disabling GitLens blame annotations feature.mp4

<br><br>
### 15. Making Unsplash API requests/:

- 15.01.1 07. BEGIN - first api call - 4dcf62d.zip
- 15.01. BEGIN - Creating account at Unsplash and registering new App.mp4
- 15.02. Adding local file with Environment Variables.mp4
- 15.03. How to make Unsplash API request and what is URL.mp4
- 15.04.1 07. END - first api call - 80036f3.zip
- 15.04. END - Making first API request for random photo.mp4
- 15.05. Analyzing API requests and responses.mp4
- 15.06.1 08. BEGIN - reset search input upon submit - 80036f3.zip
- 15.06. BEGIN - CHALLENGE - Clear search input.mp4
- 15.07.1 08. END - reset search input upon submit - 597e459.zip
- 15.07. END - CHALLENGE SOLUTION - Clear search input.mp4

<br><br>
### 16. Configuring ESLint and Prettier in the frontend app/:

- 16.01.1 09. BEGIN - eslint and prettier setup - 597e459.zip
- 16.01. BEGIN - What is code formatter.mp4
- 16.02. What is linter.mp4
- 16.03. Configure ESLint validation.mp4
- 16.04. Installing and enabling Prettier ESLint Plugin.mp4
- 16.05. Adjusting Prettier configuration.mp4
- 16.06. Enabling ESLint errors fixing on File Save.mp4
- 16.07.1 09. END - eslint and prettier setup - d02a919.zip
- 16.07. END - Adding NPM linting scripts.mp4
- 16.08.1 10. BEGIN - fixed code with linter - d02a919.zip
- 16.08.2 10. END - fixed code with linter - 2725817.zip
- 16.08. BEGIN and END - Fixing all linting errors.mp4
- 16.09.1 11. BEGIN - adjust vs code tab size - 2725817.zip
- 16.09.2 11. END - adjust vs code tab size - 4f16737.zip
- 16.09. BEGIN and END - Changing editor tabSize in VS Code.mp4
- 16.10. ESLint with Prettier setup Summary.mp4

<br><br>
### 17. Saving images in the state and deleting them in the UI/:

- 17.01.1 12. BEGIN - save images in the state - 4f16737.zip
- 17.01. BEGIN - Planning next steps in building frontend app.mp4
- 17.02. Saving images in the state.mp4
- 17.03.1 12. END - save images in the state - 8ba15d0.zip
- 17.03. END - State in React is updated asynchronously.mp4
- 17.04.1 13. BEGIN - display and delete images - 8ba15d0.zip
- 17.04. BEGIN - Adding React Developer Tools Chrome extension.mp4
- 17.05. Creating ImageCard component.mp4
- 17.06. Adding props to the ImageCard component.mp4
- 17.07. Displaying all images in the UI using map method.mp4
- 17.08. Adjust layout of the image cards.mp4
- 17.09.1 13. END - display and delete images - f23785c.zip
- 17.09. END - Adding delete images functionality.mp4

<br><br>
### 18. Adding SVG logo and Welcome component/:

- 18.01.1 14. BEGIN - added text logo - f23785c.zip
- 18.01. BEGIN - Creating SVG logo from text.mp4
- 18.02.1 14. END - added text logo - 8515f8c.zip
- 18.02. END - Editing SVG logo.mp4
- 18.03.1 15. BEGIN - welcome component - 8515f8c.zip
- 18.03.2 15. END - welcome component - dab9224.zip
- 18.03. BEGIN and END - Adding Welcome component.mp4
- 18.04. Basic frontend app Summary.mp4
- 18.05. Basic frontend app codebase review.mp4
- 18.06.1 16. BEGIN - update react-scripts version - dab9224.zip
- 18.06.2 16. END - update react-scripts version - da0c798.zip
- 18.06. BEGIN and END - Updating create-react-app application.mp4

<br><br>
### 19. REST API and HTTP Methods/:

- 19.01. Which problems API does solve.mp4
- 19.02. Client-server communication.mp4
- 19.03. REST API.mp4
- 19.04. URL is unique resource identifier.mp4
- 19.05. Requests and Responses.mp4
- 19.06. Analyzing requests and responses in our Frontend app.mp4
- 19.07. HTTP Methods Overview.mp4
- 19.08. CRUD Operations.mp4
- 19.09. Idempotent HTTP Methods.mp4
- 19.10. HTTP Response Status Codes.mp4
- 19.11. Most common Success and Redirect HTTP status codes.mp4
- 19.12. Most common Client and Server Error status codes.mp4
- 19.13. REST API and HTTP methods Summary.mp4

<br><br>
### 20. API Service Overview and Python installation/:

- 20.01. Frontend and API services Overview.mp4
- 20.02. Installing Python, Pip and Pipenv on MacOS.mp4
- 20.03. Installing Python, Pip and Pipenv on Windows.mp4
- 20.04. Python Flask API Implementation Steps.mp4

<br><br>
### 21. Creating Python Virtual Environment for api application using pipenv/:

- 21.01.1 17. BEGIN - initial api and python virtual environment - da0c798.zip
- 21.01. BEGIN - Creating api folder and running basic Python app.mp4
- 21.02. Creating Python virtualenv and installing Flask.mp4
- 21.03.1 17. END - initial api and python virtual environment - 9187dd0.zip
- 21.03. END - Exploring Python virtualenv and changing VS Code Python interpreter.mp4

<br><br>
### 22. Creating and starting simple Flask web server/:

- 22.01. NOTE for Windows Users.html
- 22.02.1 18. BEGIN - sample flask app - 9187dd0.zip
- 22.02. BEGIN - Simple Flask application.mp4
- 22.03. Imports in Python and __name__.mp4
- 22.04. Import from other Python module and __name__.mp4
- 22.05. Decorators in Python.mp4
- 22.06.1 18. END - sample flask app - c52342e.zip
- 22.06. END - Starting Flask app inside of the module.mp4
- 22.07. Consistent launch of python modules on Mac and Windows.mp4

<br><br>
### 23. Creating new-image Flask API endpoint and testing it using Postman/:

- 23.01.1 19. BEGIN - added request to the unsplash api - c52342e.zip
- 23.01. BEGIN - Creating new-image API endpoint.mp4
- 23.02. Testing new-image API endpoint using Postman.mp4
- 23.03. Making API request to the Unsplash API.mp4
- 23.04. Finalize and test API request to the Unsplash API.mp4

<br><br>
### 24. Importing env variables from the file in the Python app/:

- 24.01. Extracting UNSPLASH_KEY to the env file.mp4
- 24.02.1 19. END - added request to the unsplash api - 9b9feef.zip
- 24.02. END - Verifying usage of the env variables from the file.mp4
- 24.03.1 20. BEGIN - enable debug mode in flask app - 9b9feef.zip
- 24.03.2 20. END - enable debug mode in flask app - 33362dc.zip
- 24.03. BEGIN and END - Enabling Debug mode in the Flask app.mp4

<br><br>
### 25. Configuring frontend app to make new image API requests via Flask API/:

- 25.01.1 21. BEGIN - changed api in the frontend and enabled cors in api - 33362dc.zip
- 25.01. BEGIN - Changing API in the frontend React app.mp4
- 25.02. Trying to test Frontend app with Flask API.mp4

<br><br>
### 26. CORS and enabling CORS in the Flask app/:

- 26.01. What is CORS.mp4
- 26.02. Comparing responses from the Flask API and Unsplash API.mp4
- 26.03.1 21. END - changed api in the frontend and enabled cors in api - 9488fe0.zip
- 26.03. END - Enabling CORS in the Flask app.mp4
- 26.04.1 22. BEGIN - enabled pylint and black code formatter in python - 9488fe0.zip
- 26.04. BEGIN - Installing and enabling Pylint linter and Black formatter.mp4
- 26.05.1 22. END - enabled pylint and black code formatter in python - 75e20c0.zip
- 26.05. END - Enabling formatting on save and fixing all mistakes in the Python app.mp4
- 26.06. Basic API Summary.mp4

<br><br>
### 27. Database integration plan/:

- 27.01. It is time to save images data to the database.mp4
- 27.02. Application structure with database.mp4
- 27.03. Implementation steps for saving images data in the database.mp4
- 27.04. All API endpoints in the current implementation phase.mp4

<br><br>
### 28. Dockerizing Backend Flask API Service/:

- 28.01. Installing Docker and hello-world using Docker.mp4
- 28.02.1 23. BEGIN - dockerfile for the api service - 75e20c0.zip
- 28.02. BEGIN - Creating Dockerfile for the Python API service.mp4
- 28.03. Building Docker image for the API service.mp4
- 28.04. Running API containers based on the built Docker image.mp4
- 28.05. Analyzing API Docker container from inside.mp4
- 28.06.1 23. END - dockerfile for the api service - 6985753.zip
- 28.06. END - Docker image for the API service Summary.mp4

<br><br>
### 29. Dockerizing Frontend React Application/:

- 29.01.1 24. BEGIN - dockerfile for the frontend service - 6985753.zip
- 29.01. BEGIN - Creating Dockerfile for the frontend application.mp4
- 29.02. Building Docker image for the frontend app.mp4
- 29.03.1 24. END - dockerfile for the frontend service - 9d68416.zip
- 29.03. END - Running Docker container for the frontend service.mp4
- 29.04. Exploring frontend container from inside.mp4
- 29.05. Why you need to have node_modules folder and python venv folder locally.mp4
- 29.06. Running both frontend and api containers in background.mp4

<br><br>
### 30. Docker Compose for the API and Frontend services/:

- 30.01.1 25. BEGIN - initial version of the docker-compose - 9d68416.zip
- 30.01. BEGIN - Creating basic docker-compose file.mp4
- 30.02. Bringing up both containers using docker-compose.mp4
- 30.03.1 25. END - initial version of the docker-compose - 3984483.zip
- 30.03. END - How to operate containers using docker-compose.mp4
- 30.04.1 26. BEGIN - docker-compose for frontend and api - 3984483.zip
- 30.04. BEGIN - Creating volumes mapping for the frontend service.mp4
- 30.05. Fix volumes sync in the React container.mp4
- 30.06. Enabling volumes mapping for the api service.mp4
- 30.07.1 26. END - docker-compose for frontend and api - 4329c8d.zip
- 30.07. END - Enabling auto-restart and docker-compose Summary.mp4

<br><br>
### 31. Adding MongoDB to the Docker Compose/:

- 31.01. Why do we need mongo and mongo-express services.mp4
- 31.02. Mongo and mongo-express official Docker images Overview.mp4
- 31.03.1 27. BEGIN - added mongo and mongo-express to the docker-compose - 4329c8d.zip
- 31.03. BEGIN - Adding mongo and mongo-express services to the docker-compose file.mp4
- 31.04. Starting all services including mongo and mongo-express using docker-compose.mp4
- 31.05. Docker Desktop Overview.mp4
- 31.06. Using MongoDB shell and mongo-express GUI.mp4

<br><br>
### 32. Persistent volume for MongoDB and Docker networking/:

- 32.01. MongoDB data is now deleted after docker-compose restart.mp4
- 32.02. Configuring persistent data volume for the mongo container.mp4
- 32.03. Verifying persistent MongoDB storage using volume.mp4
- 32.04.1 27. END - added mongo and mongo-express to the docker-compose - 9d81077.zip
- 32.04. END - Mongo and mongo-express setup Summary.mp4
- 32.05. How docker-compose containers communicate with each other.mp4
- 32.06. Exploring networking between Docker containers.mp4

<br><br>
### 33. API endpoint for reading and creating images in the database/:

- 33.01. Plan for the integration of the MongoDB and Flask API Service.mp4
- 33.02.1 28. BEGIN - pymongo and mongo test - 9d81077.zip
- 33.02. BEGIN - Installing pymongo.mp4
- 33.03. Creating instance of the MongoClient.mp4
- 33.04. Adding insertion of the document to the MongoDB by Python API service.mp4
- 33.05. Rebuilding api service Docker image.mp4
- 33.06.1 28. END - pymongo and mongo test - 0f798bd.zip
- 33.06. END - Pymongo and mongo connectivity testing Summary.mp4
- 33.07.1 29. BEGIN - basic images endpoint with get and post methods - 0f798bd.zip
- 33.07. BEGIN - New API endpoint for reading and creating images in the database.mp4
- 33.08. Adding images endpoint and handling GET requests.mp4
- 33.09. Handling POST requests to the images endpoint.mp4
- 33.10. Testing GET and POST methods in the images API.mp4
- 33.11. Fixing errors in the images API.mp4
- 33.12.1 29. END - basic images endpoint with get and post methods - fcb2d70.zip
- 33.12. END - Finalize with testing of the images API after errors fixing.mp4
- 33.13. Images API endpoint Summary.mp4

<br><br>
### 34. Saving and reading images from the database in the frontend app/:

- 34.01. Save and get images in the frontend implementation Overview.mp4
- 34.02.1 30. BEGIN - introduce axios and rewrite new-image fetch call - fcb2d70.zip
- 34.02. BEGIN - Installing Axios in the frontend app and rebuilding frontend image.mp4
- 34.03. Start of the replacement of the fetch with Axios.mp4
- 34.04.1 30. END - introduce axios and rewrite new-image fetch call - df5332d.zip
- 34.04. END - Continue replacement of the fetch with Axios.mp4
- 34.05.1 31. BEGIN - get saved images in the frontend - df5332d.zip
- 34.05. BEGIN - Adding some images to the Mongo database using Postman.mp4
- 34.06. Retrieving saved images when React app loads.mp4
- 34.07.1 31. END - get saved images in the frontend - 33af9cb.zip
- 34.07. END - Examining how useEffect works.mp4
- 34.08.1 32. BEGIN - eslint config fix to enable packages location - 33af9cb.zip
- 34.08.2 32. END - eslint config fix to enable packages location - 8bc3761.zip
- 34.08. BEGIN and END - Explaining ESLint error and extending ESLint config.mp4
- 34.09.1 33. BEGIN - saving images in the frontend app - 8bc3761.zip
- 34.09. BEGIN - Saving images in the database from the frontend app.mp4
- 34.10.1 33. END - saving images in the frontend app - 653a224.zip
- 34.10. END - Hiding Save button for already saved images.mp4
- 34.11. Saving images in the database Summary.mp4

<br><br>
### 35. Deleting images and frontend improvements/:

- 35.01. Plan for the improvements section.mp4
- 35.02. How to handle deletion of the images.mp4
- 35.03.1 34. BEGIN - single image endpoint with delete method enabled - 653a224.zip
- 35.03. BEGIN - Delete image API endpoint CHALLENGE.mp4
- 35.04. Delete image API endpoint SOLUTION.mp4
- 35.05.1 34. END - single image endpoint with delete method enabled - 94a31d9.zip
- 35.05. END - Error handling for the delete image API endpoint.mp4
- 35.06.1 35. BEGIN - delete image request from the frontend - 94a31d9.zip
- 35.06. BEGIN - Delete image request in the frontend CHALLENGE.mp4
- 35.07.1 35. END - delete image request from the frontend - e0fcecc.zip
- 35.07. END - Delete image request in the frontend SOLUTION.mp4
- 35.08.1 36. BEGIN - spinner in the frontend - e0fcecc.zip
- 35.08. BEGIN - Spinner CHALLENGE.mp4
- 35.09. Spinner SOLUTION.mp4
- 35.10.1 36. END - spinner in the frontend - 88005b3.zip
- 35.10. END - Finalize with Spinner SOLUTION.mp4
- 35.11.1 37. BEGIN - image author info - 88005b3.zip
- 35.11. BEGIN - Image Author Information CHALLENGE.mp4
- 35.12.1 37. END - image author info - e6fd8af.zip
- 35.12. END - Image Author Information SOLUTION.mp4
- 35.13.1 38. BEGIN - toast notifications for the frontend app - e6fd8af.zip
- 35.13. BEGIN - Toast notifications CHALLENGE.mp4
- 35.14. Toast notifications SOLUTION.mp4
- 35.15.1 38. END - toast notifications for the frontend app - 73c7f4c.zip
- 35.15. END - Finalize with toast notifications SOLUTION.mp4
- 35.16. Improvements Challenges section Summary.mp4
